import autoinstrument
from opentelemetry.sdk.trace.export import SpanExporter
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
import pytest


def test_initialized_otlp_endpoint_not_specified():
    ai = autoinstrument.AutoInstrument()
    assert isinstance(ai.trace_exporter, OTLPSpanExporter)


def test_initialized_otlp_endpoint_specified():
    exporter = SpanExporter()
    ai = autoinstrument.AutoInstrument(trace_exporter=exporter)
    assert ai.trace_exporter == exporter


def test_expect_error_if_initialize_with_non_exporter():
    class NotAnExporter:
        pass

    with pytest.raises(TypeError):
        autoinstrument.AutoInstrument(exporter=NotAnExporter())


def test_docstring_added_to_function():
    def a_function():
        """This is a docstring"""

    ai = autoinstrument.AutoInstrument()
    ai(a_function)
    assert (
        a_function.__doc__
        == "This is a docstring" + "\n" + autoinstrument.INSTRUMENTETION_DOC_LINE
    )
