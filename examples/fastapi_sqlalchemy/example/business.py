from sqlalchemy import select
from . import schemas, db


class NoResult(Exception):
    pass


def create_item(item: schemas.ItemCreate, session: db.Session):
    with session.begin():
        db_item = db.Item(**item.dict())
        session.add(db_item)
        session.commit()
    return schemas.ItemRead.from_orm(db_item)


def get_items(session: db.Session):
    query = select(db.Item)
    results = session.execute(query).scalars().all()
    return [schemas.ItemRead.from_orm(result) for result in results]


def _get_db_item(id: int, session: db.Session) -> db.Item:
    query = select(db.Item).where(db.Item.id == id)
    result = session.execute(query).scalars().one_or_none()
    if result is None:
        raise (NoResult(f"No item found with id={id}"))
    return result


def get_item(id: int, session: db.Session):
    result = _get_db_item(id, session)
    return schemas.ItemRead.from_orm(result)


def update_item(id: int, item: schemas.ItemUpdate, session: db.Session):
    with session.begin():
        db_item = _get_db_item(id, session)
        for key, value in item.dict().items():
            setattr(db_item, key, value)
        session.commit()
    return schemas.ItemRead.from_orm(db_item)


def update_item(id: int, item: schemas.ItemUpdate, session: db.Session):
    with session.begin():
        db_item = _get_db_item(id, session)
        for key, value in item.dict().items():
            setattr(db_item, key, value)
        session.commit()
    return schemas.ItemRead.from_orm(db_item)


def delete_item(id: int, session: db.Session):
    with session.begin():
        session.delete(_get_db_item(id, session))
        session.commit()
