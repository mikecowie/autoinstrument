from autoinstrument import AutoInstrument
from . import business, app


FUNCTIONS_TO_INSTRUMENT = (
    business.create_item,
    business.get_item,
    business.get_items,
    business.update_item,
    business.delete_item,
)

ai = AutoInstrument()


def instrument():
    ai(*FUNCTIONS_TO_INSTRUMENT)
