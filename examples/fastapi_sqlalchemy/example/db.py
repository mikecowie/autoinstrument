from typing import TYPE_CHECKING
from datetime import datetime
import sqlalchemy as sa
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, DeclarativeBase, Mapped
from sqlalchemy.orm import Session as Session_


engine = create_engine("sqlite:///items.db")
Session = sessionmaker(bind=engine)
if TYPE_CHECKING:
    Session = Session_


class Base(DeclarativeBase):
    pass


class Item(Base):
    __tablename__ = "item"
    id: Mapped[int] = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    name: Mapped[str] = sa.Column(sa.Text, nullable=False, unique=False)
    detail: Mapped[str] = sa.Column(sa.Text, nullable=False, unique=False)
    created: Mapped[datetime] = sa.Column(sa.DateTime, default=datetime.now)
    updated: Mapped[datetime] = sa.Column(
        sa.DateTime, default=datetime.now, onupdate=datetime.now
    )


Base.metadata.create_all(engine)
