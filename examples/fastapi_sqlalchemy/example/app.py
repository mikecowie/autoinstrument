from typing import Callable
from fastapi import FastAPI, Depends, status, Request
from fastapi.responses import JSONResponse

import autoinstrument


from . import schemas, business, db, depends


app = FastAPI()


def item_hyperlinker(
    request: Request, item: schemas.ItemRead
) -> schemas.HyperlinkedItemRead:
    href = request.url_for("get_item", id=item.id)
    return schemas.HyperlinkedItemRead.parse_obj(dict(href=str(href)) | item.dict())


@app.exception_handler(business.NoResult)
def handle_no_result(_, exc: business.NoResult):
    return JSONResponse(
        content=dict(detail=str(exc)), status_code=status.HTTP_404_NOT_FOUND
    )


@app.post(
    "/items/",
    response_model=schemas.HyperlinkedItemRead,
    status_code=status.HTTP_201_CREATED,
)
def create_item(
    request: Request,
    item: schemas.ItemCreate,
    session: db.Session = Depends(depends.get_db_session),
) -> schemas.HyperlinkedItemRead:
    item = business.create_item(item, session)
    return item_hyperlinker(request, item)


@app.get("/items/", response_model=list[schemas.HyperlinkedItemRead])
def get_items(
    request: Request,
    session: db.Session = Depends(depends.get_db_session),
) -> list[schemas.HyperlinkedItemRead]:
    return [item_hyperlinker(request, item) for item in business.get_items(session)]


@app.get("/items/{id}", response_model=schemas.HyperlinkedItemRead)
def get_item(
    request: Request, id: int, session: db.Session = Depends(depends.get_db_session)
) -> schemas.HyperlinkedItemRead:
    item = business.get_item(id, session)
    return item_hyperlinker(request, item)


@app.put("/items/{id}", response_model=schemas.HyperlinkedItemRead)
def update_item(
    request: Request,
    id: int,
    item: schemas.ItemUpdate,
    session: db.Session = Depends(depends.get_db_session),
) -> schemas.HyperlinkedItemRead:
    item = business.update_item(id, item, session)
    return item_hyperlinker(request, item)


@app.delete("/items/{id}", status_code=status.HTTP_204_NO_CONTENT)
def update_item(id: int, session: db.Session = Depends(depends.get_db_session)) -> None:
    return business.delete_item(id, session)
