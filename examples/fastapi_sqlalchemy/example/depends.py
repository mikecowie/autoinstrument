from .db import Session


def get_db_session() -> Session:
    return Session()
