from datetime import datetime
from pydantic import BaseModel, AnyHttpUrl


class ItemCreate(BaseModel):
    name: str
    detail: str


class ItemUpdate(BaseModel):
    name: str
    detail: str


class ItemRead(BaseModel):
    id: int
    name: str
    detail: str
    created: datetime
    updated: datetime

    class Config:
        orm_mode = True


class HyperlinkedItemRead(BaseModel):
    href: AnyHttpUrl
    name: str
    detail: str
    created: datetime
    updated: datetime
