from functools import wraps
import os
import sys
from typing import Callable
from opentelemetry.sdk.trace.export import SpanExporter
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry import trace


INSTRUMENTETION_DOC_LINE = "Instrumented for opentelemetry by autoinstrument"


class AutoInstrument:
    """Automatically sets up instrumentation of python callables

    Args:
        trace_exporter: optional instance of opentelemetry.sdk.trace.exporter.SpanExporter to use instead of the default

    Example:
        >>> ai = AutoInstrument()
        >>> ai()

    """

    def __init__(self, trace_exporter: SpanExporter | None = None):
        if trace_exporter is None:
            trace_endpoint = os.environ.get(
                "OTEL_EXPORTER_OTLP_ENDPOINT", "http://localhost:4317"
            )
            self.trace_exporter = OTLPSpanExporter(endpoint=trace_endpoint)
        else:
            self.trace_exporter = trace_exporter

    def __call__(self, *args):
        for arg in args:
            if callable(arg):
                module_dict = sys.modules[arg.__module__].__dict__
                module_dict[arg.__name__] = self._trace_function(arg)

    @staticmethod
    def _trace_function(func: Callable):
        span_name = f"{func.__module__}.{func.__name__}"
        # mark the function as documented by appending to its docstring
        func.__doc__ = func.__doc__ + "\n" + INSTRUMENTETION_DOC_LINE

        @wraps(func)
        def _inner(*args, **kwargs):
            with trace.get_tracer(__module__).start_as_current_span(span_name) as span:
                span.set_attribute(key="call.args", value=str(args))
                for key, value in kwargs.items():
                    span.set_attribute(key=f"call.kwargs.{key}", value=repr(value))
                return func(*args, **kwargs)

        return _inner
